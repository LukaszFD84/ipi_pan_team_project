﻿** Highlights
     Tables that will be rebuilt
       None
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       None

** User actions
     Drop
       [library].[CHK_NAME] (Check Constraint)
     Create
       [library].[CHK_NAME] (Check Constraint)

** Supporting actions
